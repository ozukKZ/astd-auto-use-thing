spawn(function()
game.StarterGui:SetCore("SendNotification", {
Title = "HIDE this";
Text = "Press [ to hide",
Icon = "rbxassetid://5472203252";
Duration = 15;
})
game.StarterGui:SetCore("SendNotification", {
Title = "CHOOSE";
Text = "Which one are you playing",
Icon = "rbxassetid://5472203252";
Duration = 15;
})

local ScreenGui = Instance.new("ScreenGui")
local Frame = Instance.new("Frame")
local Speedx1 = Instance.new("TextButton")
local Speedx2 = Instance.new("TextButton")
local Speedx3 = Instance.new("TextButton")
local Frame_2 = Instance.new("Frame")

--Properties:

ScreenGui.Parent = game.CoreGui
ScreenGui.ZIndexBehavior = Enum.ZIndexBehavior.Sibling

Frame.Parent = ScreenGui
Frame.BackgroundColor3 = Color3.fromRGB(70, 70, 70)
Frame.BorderSizePixel = 0
Frame.Position = UDim2.new(0.5817941949, 0, 0.031096994, 0)
Frame.Size = UDim2.new(0, 150, 0, 100)
Frame.Draggable = true 
Frame.Active = true 

Speedx1.Name = "Speedx1"
Speedx1.Parent = Frame
Speedx1.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
Speedx1.BorderSizePixel = 0
Speedx1.Position = UDim2.new(0.0180065946, 0, 0.053617444, 0)
Speedx1.Size = UDim2.new(0, 120, 0, 30)
Speedx1.Style = Enum.ButtonStyle.RobloxRoundButton
Speedx1.Font = Enum.Font.SourceSans
Speedx1.Text = "|Merlin|Game Speed x1"
Speedx1.TextColor3 = Color3.fromRGB(255, 255, 255)
Speedx1.TextSize = 14.000
Speedx1.MouseButton1Down:connect(function()
loadstring(game:HttpGet("https://gitlab.com/ozukKZ/astd-auto-use-thing/-/raw/master/Merlin%20buff%20x1.lua",true))()
end)

Speedx2.Name = "Speedx2"
Speedx2.Parent = Frame
Speedx2.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
Speedx2.BorderSizePixel = 0
Speedx2.Position = UDim2.new(0.0180065946, 0, 0.343617444, 0)
Speedx2.Size = UDim2.new(0, 120, 0, 30)
Speedx2.Style = Enum.ButtonStyle.RobloxRoundButton
Speedx2.Font = Enum.Font.SourceSans
Speedx2.Text = "|Merlin|Game Speed x2"
Speedx2.TextColor3 = Color3.fromRGB(255, 255, 255)
Speedx2.TextSize = 14.000
Speedx2.MouseButton1Down:connect(function()
loadstring(game:HttpGet("https://gitlab.com/ozukKZ/astd-auto-use-thing/-/raw/master/Merlin%20buff%20x2.lua",true))()
end)

Speedx3.Name = "Speedx3"
Speedx3.Parent = Frame
Speedx3.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
Speedx3.BorderSizePixel = 0
Speedx3.Position = UDim2.new(0.0180065946, 0, 0.633617444, 0)
Speedx3.Size = UDim2.new(0, 120, 0, 30)
Speedx3.Style = Enum.ButtonStyle.RobloxRoundButton
Speedx3.Font = Enum.Font.SourceSans
Speedx3.Text = "|Merlin|Game Speed x3"
Speedx3.TextColor3 = Color3.fromRGB(255, 255, 255)
Speedx3.TextSize = 14.000
Speedx3.MouseButton1Down:connect(function()
loadstring(game:HttpGet("https://gitlab.com/ozukKZ/astd-auto-use-thing/-/raw/master/Merlin%20buff%20x3.lua",true))()
end)

Frame_2.Parent = Frame
Frame_2.BackgroundColor3 = Color3.fromRGB(255, 26, 26)
Frame_2.BorderSizePixel = 0
Frame_2.Size = UDim2.new(0, 150, 0, 7)


--scripts

local Frame = -- GUI
	game:GetService("Players").LocalPlayer:GetMouse().KeyDown:Connect(function(Key)
	if Key == "[" then
		Frame.Visible = not Frame.Visible
	end
end)

end)
