wait()
game.StarterGui:SetCore("SendNotification", {
Title = "TP mode";
Text = "LOADING....<3",
Icon = "rbxassetid://5472203252";
Duration = 2;
})
game.StarterGui:SetCore("SendNotification", {
Title = "TPto";
Text = "Which one you want go to",
Icon = "rbxassetid://5472203252";
Duration = 6;
})
local ScreenGui = Instance.new("ScreenGui")
local Frame = Instance.new("Frame")
local Story = Instance.new("TextButton")
local Frame_2 = Instance.new("Frame")
local Inf = Instance.new("TextButton")
local Farming = Instance.new("TextButton")
local OrbsTP = Instance.new("TextButton")
local AFK = Instance.new("TextButton")
--Properties:

ScreenGui.Parent = game.CoreGui
ScreenGui.ZIndexBehavior = Enum.ZIndexBehavior.Sibling

Frame.Parent = ScreenGui
Frame.BackgroundColor3 = Color3.fromRGB(70, 70, 70)
Frame.BorderSizePixel = 0
Frame.Position = UDim2.new(0.7817941949, 0, 0.031096994, 0)
Frame.Size = UDim2.new(0, 170, 0, 89)
Frame.Draggable = true 
Frame.Active = true 

Story.Name = "Story"
Story.Parent = Frame
Story.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
Story.BorderSizePixel = 0
Story.Position = UDim2.new(0.0180065946, 0, 0.213617444, 0)
Story.Size = UDim2.new(0, 55, 0, 22)
Story.Style = Enum.ButtonStyle.RobloxRoundButton
Story.Font = Enum.Font.SourceSans
Story.Text = "Story"
Story.TextColor3 = Color3.fromRGB(255, 255, 255)
Story.TextSize = 14.000
Story.MouseButton1Down:connect(function()
game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = CFrame.new(24.64541053772, 1386.9996337891, 806.19879150391)
end)
Frame_2.Parent = Frame
Frame_2.BackgroundColor3 = Color3.fromRGB(255, 26, 26)
Frame_2.BorderSizePixel = 0
Frame_2.Size = UDim2.new(0, 170, 0, 7)

Inf.Name = "Inf"
Inf.Parent = Frame
Inf.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
Inf.BorderSizePixel = 0
Inf.Position = UDim2.new(0.344620593, 0, 0.213617444, 0)
Inf.Size = UDim2.new(0, 55, 0, 22)
Inf.Style = Enum.ButtonStyle.RobloxRoundButton
Inf.Font = Enum.Font.SourceSans
Inf.Text = "Inf"
Inf.TextColor3 = Color3.fromRGB(255, 255, 255)
Inf.TextSize = 14.000
Inf.MouseButton1Down:connect(function()
game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = CFrame.new(-111.81027984619, 1386.9996337891, 807.70343017578)
end)
Farming.Name = "Farming"
Farming.Parent = Frame
Farming.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
Farming.BorderSizePixel = 0
Farming.Position = UDim2.new(0.664620593, 0, 0.213617444, 0)
Farming.Size = UDim2.new(0, 55, 0, 22)
Farming.Style = Enum.ButtonStyle.RobloxRoundButton
Farming.Font = Enum.Font.SourceSans
Farming.Text = "Farming"
Farming.TextColor3 = Color3.fromRGB(255, 255, 255)
Farming.TextSize = 14.000
Farming.MouseButton1Down:connect(function()
game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = CFrame.new(-954.32836914063, 42.966556549072, -886.66253662109)
end)
OrbsTP.Name = "OrbsTP"
OrbsTP.Parent = Frame
OrbsTP.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
OrbsTP.BorderSizePixel = 0
OrbsTP.Position = UDim2.new(0.0180065946, 0, 0.513617444, 0)
OrbsTP.Size = UDim2.new(0, 55, 0, 22)
OrbsTP.Style = Enum.ButtonStyle.RobloxRoundButton
OrbsTP.Font = Enum.Font.SourceSans
OrbsTP.Text = "OrbsTP"
OrbsTP.TextColor3 = Color3.fromRGB(255, 255, 255)
OrbsTP.TextSize = 14.000
OrbsTP.MouseButton1Down:connect(function()
game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = CFrame.new(-859.75964355469, 46.966651916504, -790.01190185547)
end)

AFK.Name = "AFK place"
AFK.Parent = Frame
AFK.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
AFK.BorderSizePixel = 0
AFK.Position = UDim2.new(0.344620593, 0, 0.513617444, 0)
AFK.Size = UDim2.new(0, 55, 0, 22)
AFK.Style = Enum.ButtonStyle.RobloxRoundButton
AFK.Font = Enum.Font.SourceSans
AFK.Text = "AFK place"
AFK.TextColor3 = Color3.fromRGB(255, 255, 255)
AFK.TextSize = 14.000
AFK.MouseButton1Down:connect(function()
game:GetService("TeleportService"):Teleport(5552815761, game.Players.LocalPlayer)
end)

--scripts

local Frame = -- GUI
	game:GetService("Players").LocalPlayer:GetMouse().KeyDown:Connect(function(Key)
	if Key == "p" then
		Frame.Visible = not Frame.Visible
	end
end)
