spawn(function()
game.StarterGui:SetCore("SendNotification", {
Title = "Speed x3";
Text = "place 4 or 8 max upgrade, do not change speed game",
Icon = "rbxassetid://1019139558";
Duration = 35;
})
end)
--
local me = game.Players.LocalPlayer
local remote = game.ReplicatedStorage.Remotes.Input
local Brook61 = {}
local Brook62 = {}
local Brook63 = {}

for _,v in pairs(game:GetService("Workspace").Unit:GetChildren()) do
    if v.Name == 'Brook6' and v.Owner.Value == me then
        table.insert(Brook61, v)
    end
end

if #Brook61 == 4 then --change 15 to 16
    while true do
        remote:FireServer('UseSpecialMove', Brook61[1])
        wait(5.2)
        remote:FireServer('UseSpecialMove', Brook61[3])
        wait(5.2)
        remote:FireServer('UseSpecialMove', Brook61[2])
        wait(5.2)
        remote:FireServer('UseSpecialMove', Brook61[4])
        wait(5.2)
    end
elseif #Brook61 == 8 then
    for i = 1, 4 do
        table.insert(Brook62, Brook61[1])
        table.remove(Brook61, 1)
    end
    for i = 1, 4 do
        table.insert(Brook63, Brook61[1])
        table.remove(Brook61, 1)
    end
    while true do
        remote:FireServer('UseSpecialMove', Brook62[1]) -- 14 - 15
        wait(1)
        remote:FireServer('UseSpecialMove', Brook63[1])
        wait(5.2)
        remote:FireServer('UseSpecialMove', Brook62[3])
        wait(1)
        remote:FireServer('UseSpecialMove', Brook63[3])
        wait(5.2)
        remote:FireServer('UseSpecialMove', Brook62[2])
        wait(1)
        remote:FireServer('UseSpecialMove', Brook63[2])
        wait(5.2)
        remote:FireServer('UseSpecialMove', Brook62[4])
        wait(1)
        remote:FireServer('UseSpecialMove', Brook63[4])
        wait(5.2)
    end
elseif #Brook61 > 4 and #Brook61 < 8 then
    repeat wait(1)
        table.remove(Brook61, 1)
    until #Brook61 == 4
    while true do
        remote:FireServer('UseSpecialMove', Brook61[1]) -- 15 - 16
        wait(5.2)
        remote:FireServer('UseSpecialMove', Brook61[3])
        wait(5.2)
        remote:FireServer('UseSpecialMove', Brook61[2])
        wait(5.2) -- .6
        remote:FireServer('UseSpecialMove', Brook61[4])
        wait(5.2)
    end
end
