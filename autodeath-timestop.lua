game.StarterGui:SetCore("SendNotification", {
Title = "Upgrade all";
Text = "Right Ctrl to Upgrade all",
Icon = "rbxassetid://5472203252";
Duration = 65;
})
game.StarterGui:SetCore("SendNotification", {
Title = "GOJO";
Text = "Left Ctrl + MouseLeft to time stop",
Icon = "rbxassetid://5472203252";
Duration = 65;
})
game.StarterGui:SetCore("SendNotification", {
Title = "DEATH NOTE";
Text = "Spam F1 to use Light Yagami",
Icon = "rbxassetid://5472203252";
Duration = 65;
})
spawn(function()
time_stop_unit = 'Satoru Gojou'

--Right Control         to Upgrade all
--Left Control + M1     to time stop
--F1                    to use Light Yagami
repeat wait() until game.Loaded
wait(15)
if game.PlaceId == 4996049426 and not game.Workspace:FindFirstChild'Queue' then
local me = game.Players.LocalPlayer
local mouse = me:GetMouse()
local remote = game.ReplicatedStorage.Remotes.Input
local timestop

game:GetService("Workspace").Unit.ChildAdded:Connect(function(x)
wait(.1)
if x.Name == time_stop_unit and x.Owner.Value == me then
timestop = x
end
end)

game:GetService("UserInputService").InputBegan:Connect(function(input)
if input.KeyCode == Enum.KeyCode.RightControl then-- upgrade
for _,v in pairs(game:GetService("Workspace").Unit:GetChildren()) do
if v.Owner.Value == me and v.UpgradeTag.Value ~= v.MaxUpgradeTag.Value and me.Money.Value > 190 then
for i = 1, 6 do
remote:FireServer('Upgrade', v)
wait(.24)
if v.UpgradeTag.Value == v.MaxUpgradeTag.Value then break end
end
end
end
elseif input.KeyCode == Enum.KeyCode.F1 then-- use light yagami
for _,v in pairs(game:GetService("Workspace").Unit:GetChildren()) do
if v.Name == 'Light Yagami' and v.Owner.Value == me and v.SpecialMove.Special_Enabled2.Value == false then
remote:FireServer('UseSpecialMove', v)
break
end
end
end
end)

mouse.Button1Down:connect(function()-- timestop
if game:GetService("UserInputService"):IsKeyDown(Enum.KeyCode.LeftControl) then
remote:FireServer('Summon', {
["Rotation"] = 0, 
["cframe"] = CFrame.new(mouse.hit.p),
["Unit"] = time_stop_unit
})
wait(.2)

repeat remote:FireServer('Upgrade', timestop)
wait(.24)
until timestop.UpgradeTag.Value == timestop.MaxUpgradeTag.Value

remote:FireServer('UseSpecialMove', timestop)
wait(2)
remote:FireServer('Sell', timestop)
end
end)
end
end)
----------------
