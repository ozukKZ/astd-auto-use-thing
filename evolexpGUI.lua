game.StarterGui:SetCore("SendNotification", {
Title = "Evolve exp";
Text = "LOADING....<3",
Icon = "rbxassetid://9670869807";
Duration = 3;
})

local ScreenGui = Instance.new("ScreenGui")
local Frame = Instance.new("Frame")
local Exp1 = Instance.new("TextButton")
local Frame_2 = Instance.new("Frame")
local Exp2 = Instance.new("TextButton")
local Exp3 = Instance.new("TextButton")
--local Exp4 = Instance.new("TextButton")

--Properties:

ScreenGui.Parent = game.CoreGui
ScreenGui.ZIndexBehavior = Enum.ZIndexBehavior.Sibling

Frame.Parent = ScreenGui
Frame.BackgroundColor3 = Color3.fromRGB(70, 70, 70)
Frame.BorderSizePixel = 0
Frame.Position = UDim2.new(0.6817941949, 0, 0.091096994, 0)
Frame.Size = UDim2.new(0, 150, 0, 90) --size ui
Frame.Draggable = true 
Frame.Active = true 

Exp1.Name = "Exp1"
Exp1.Parent = Frame
Exp1.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
Exp1.BorderSizePixel = 0
Exp1.Position = UDim2.new(0.0180065946, 0, 0.213617444, 0)
Exp1.Size = UDim2.new(0, 55, 0, 22)
Exp1.Style = Enum.ButtonStyle.RobloxRoundButton
Exp1.Font = Enum.Font.SourceSans
Exp1.Text = "Exp3*"
Exp1.TextColor3 = Color3.fromRGB(255, 255, 255)
Exp1.TextSize = 14.000
Exp1.MouseButton1Down:connect(function()
loadstring(game:HttpGet("https://gitlab.com/ozukKZ/astd-auto-use-thing/-/raw/master/autoEXP1.lua"))()
end)
Frame_2.Parent = Frame
Frame_2.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
Frame_2.BorderSizePixel = 0
Frame_2.Size = UDim2.new(0, 150, 0, 7) -- color ui size

Exp2.Name = "Exp2"
Exp2.Parent = Frame
Exp2.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
Exp2.BorderSizePixel = 0
Exp2.Position = UDim2.new(0.444620593, 0, 0.213617444, 0)
Exp2.Size = UDim2.new(0, 55, 0, 22)
Exp2.Style = Enum.ButtonStyle.RobloxRoundButton
Exp2.Font = Enum.Font.SourceSans
Exp2.Text = "Exp4*"
Exp2.TextColor3 = Color3.fromRGB(255, 255, 255)
Exp2.TextSize = 14.000
Exp2.MouseButton1Down:connect(function()
loadstring(game:HttpGet("https://gitlab.com/ozukKZ/astd-auto-use-thing/-/raw/master/autoEXP2.lua"))()
end)

Exp3.Name = "Exp3"
Exp3.Parent = Frame
Exp3.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
Exp3.BorderSizePixel = 0
Exp3.Position = UDim2.new(0.0180065946, 0, 0.553617444, 0)
Exp3.Size = UDim2.new(0, 55, 0, 22)
Exp3.Style = Enum.ButtonStyle.RobloxRoundButton
Exp3.Font = Enum.Font.SourceSans
Exp3.Text = "Exp5*"
Exp3.TextColor3 = Color3.fromRGB(255, 255, 255)
Exp3.TextSize = 14.000
Exp3.MouseButton1Down:connect(function()
loadstring(game:HttpGet("https://gitlab.com/ozukKZ/astd-auto-use-thing/-/raw/master/autoEXP3.lua"))()
end)
--[[
Exp4.Name = "Exp4"
Exp4.Parent = Frame
Exp4.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
Exp4.BorderSizePixel = 0
Exp4.Position = UDim2.new(0.444620593, 0, 0.553617444, 0)
Exp4.Size = UDim2.new(0, 55, 0, 22)
Exp4.Style = Enum.ButtonStyle.RobloxRoundButton
Exp4.Font = Enum.Font.SourceSans
Exp4.Text = "Exp6*"
Exp4.TextColor3 = Color3.fromRGB(255, 255, 255)
Exp4.TextSize = 14.000
Exp4.MouseButton1Down:connect(function()
loadstring(game:HttpGet("https://gitlab.com/ozukKZ/astd-auto-use-thing/-/raw/master/autoEXP4.lua"))()
end)
--scripts
]]

local Frame = -- GUI
	game:GetService("Players").LocalPlayer:GetMouse().KeyDown:Connect(function(Key)
	if Key == "p" then
		Frame.Visible = not Frame.Visible
	end
end)
