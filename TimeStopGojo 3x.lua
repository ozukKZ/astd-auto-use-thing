spawn(function()

game.StarterGui:SetCore("SendNotification", {
Title = "Gojo 3x";
Text = "Make sure all 8 gojo is MAXupgrade after you press this",
Icon = "rbxassetid://5472203252";
Duration = 30;
})
end)

local me = game.Players.LocalPlayer
local remote = game.ReplicatedStorage.Remotes.Input
local Gojo1 = {}
local Gojo2 = {}
local Gojo3 = {}

for _,v in pairs(game:GetService("Workspace").Unit:GetChildren()) do
    if v.Name == "Six Eyes Gojo" and v.Owner.Value == me then
        table.insert(Gojo1, v)
    end
end

if #Gojo1 == 4 then --change 15 to 16
    while true do
        remote:FireServer('UseSpecialMove', Gojo1[1])
        wait(3.95)
        remote:FireServer('UseSpecialMove', Gojo1[3])
        wait(3.95)
        remote:FireServer('UseSpecialMove', Gojo1[2])
        wait(3.95)
        remote:FireServer('UseSpecialMove', Gojo1[4])
        wait(3.95)
    end
elseif #Gojo1 == 8 then
    for i = 1, 4 do
        table.insert(Gojo2, Gojo1[1])
        table.remove(Gojo1, 1)
    end
    for i = 1, 4 do
        table.insert(Gojo3, Gojo1[1])
        table.remove(Gojo1, 1)
    end
    while true do
        remote:FireServer('UseSpecialMove', Gojo2[1]) -- 14 - 15
        wait(3.95)
        remote:FireServer('UseSpecialMove', Gojo3[1])
        wait(3.95)
        remote:FireServer('UseSpecialMove', Gojo2[3])
        wait(3.95)
        remote:FireServer('UseSpecialMove', Gojo3[3])
        wait(3.95)
        remote:FireServer('UseSpecialMove', Gojo2[2])
        wait(3.95)
        remote:FireServer('UseSpecialMove', Gojo3[2])
        wait(3.95)
        remote:FireServer('UseSpecialMove', Gojo2[4])
        wait(3.95)
        remote:FireServer('UseSpecialMove', Gojo3[4])
        wait(3.95)
    end
elseif #Gojo1 > 4 and #Gojo1 < 8 then
    repeat wait(1)
        table.remove(Gojo1, 1)
    until #Gojo1 == 4
    while true do
        remote:FireServer('UseSpecialMove', Gojo1[1]) -- 15 - 16
        wait(3.95)
        remote:FireServer('UseSpecialMove', Gojo1[3])
        wait(3.95)
        remote:FireServer('UseSpecialMove', Gojo1[2])
        wait(3.95) -- .6
        remote:FireServer('UseSpecialMove', Gojo1[4])
        wait(3.95)
    end
end
